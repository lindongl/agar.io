//
//  GameScene.swift
//  agar.io
//
//  Created by lld on 15/9/20.
//  Copyright (c) 2015年 Big Nerd Ranch. All rights reserved.
//


import SpriteKit
import CoreMotion


class GameScene: SKScene, SKPhysicsContactDelegate{
    
    //var motionManager : CMMotionManager!
    let motionManager: CMMotionManager = CMMotionManager()
    
    
    let ay = Vector3(x: 0.63, y: 0.0, z: -0.92)
    let az = Vector3(x: 0.0, y: 1.0, z: 0.0)
    let ax = Vector3.crossProduct(Vector3(x: 0.0, y: 1.0, z: 0.0),
        right: Vector3(x: 0.63, y: 0.0, z: -0.92)).normalized()
    
    let steerDeadZone = CGFloat(0.15)
    
    let blend = CGFloat(0.2)
    var lastVector = Vector3(x: 0, y: 0, z: 0)
    
    var circle = SizeableCircle()
    //background
    var bg = SKSpriteNode()
    var deltaPoint = CGPointZero
    
    // camera
    let camer:SKCameraNode = SKCameraNode()
    
    //
    var stickActive:Bool = false
    
    // the ball speed
    var circleSpeedX: CGFloat = 0
    var circleSpeedY: CGFloat = 0
    // name
    let label = SKLabelNode(fontNamed:"ArialMT")
    let base = SKSpriteNode(imageNamed: "base.png")
    let ball = SKSpriteNode(imageNamed: "ball.png")
    let button1 = SKSpriteNode(imageNamed:"Button")
    
    //bodyType
    enum BodyType:UInt32 {
        
        case circle = 1
        case levelGround = 2
        case verticalGround = 4
        
    }
    
    // error type
    enum MyError: ErrorType {
        case NotExist
    }
    
    // color
    let color = [SKColor.blackColor(),SKColor.blueColor(),SKColor.redColor(),SKColor.yellowColor()]
    let skins = ["china","usa"]
    
    var playerName: String?
    var accelerometer = false
    
    init(size: CGSize, name: String?, accelerometer: Bool) {
        if(name != nil) {
            self.playerName = name!
        }
        self.accelerometer = accelerometer
        super.init(size: size)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
//    
//    func createCircle() throws{
//        if(name != nil) {
////        circle.fillTexture = SKTexture(imageNamed: "\(name!).png")
//        }
//    }
    
    
    override func didMoveToView(view: SKView) {
//        self.anchorPoint = CGPointMake(0, 0)
//        self.position = CGPointMake(0,0)
        // add a button
        self.addChild(button1)
        button1.alpha = 1
//        button1.anchorPoint = CGPointMake(0,0)
        button1.position = CGPointMake(600, 100)
        print(button1.position.x)
        print(button1.position.y)
        button1.zPosition = 15
        button1.xScale = 0.3
        button1.yScale = 0.3
        
        physicsWorld.contactDelegate = self
        let circlex = SKShapeNode(circleOfRadius: 10) // Size of Circle
        circlex.position = CGPointMake(512, 384)  //Middle of Screen
        circlex.strokeColor = SKColor.blackColor()
        circlex.glowWidth = 1.0
        circlex.zPosition = 5
        self.addChild(circlex)
        
        circle = SizeableCircle(radius: 50, position: CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))) // Size of Circle //Middle of Screen
        circle.strokeColor = SKColor.blackColor()
        circle.glowWidth = 1.0
        circle.zPosition = 6
        print(playerName)
        circle.fillColor = SKColor.whiteColor()
    
        if(playerName != nil) {
            if(skins.contains(playerName!)) {
                print("\(playerName!).png")
                circle.fillTexture = SKTexture(imageNamed: "\(playerName!).png")
        } else {
            label.text = playerName
            label.fontSize = circle.frame.size.height / 9;
            label.fontColor = SKColor.whiteColor()
            label.zPosition = 6
            circle.addChild(label)
            circle.fillColor = color[Int(arc4random_uniform(UInt32(color.count)))]
            }
        } else {
            circle.fillColor = color[Int(arc4random_uniform(UInt32(color.count)))]
        }
        
        circle.physicsBody = SKPhysicsBody(circleOfRadius: circle.radius)
        circle.physicsBody!.dynamic = false
        circle.physicsBody!.categoryBitMask = BodyType.circle.rawValue
        circle.physicsBody!.contactTestBitMask = BodyType.verticalGround.rawValue
        circle.physicsBody!.contactTestBitMask = BodyType.levelGround.rawValue
//        circle.physicsBody!.collisionBitMask = BodyType.levelGround.rawValue
//        circle.physicsBody!.collisionBitMask = BodyType.verticalGround.rawValue
        
//        circle.physicsBody!.restitution = 1
        //circle.physicsBody!.collisionBitMask = BodyType.ground.rawValue
//        print("\(button1.position.x - circle.position.x)************")
//        print("***********")
//        print("\(button1.position.y - circle.position.y)************")
        circle.physicsBody!.dynamic = true
        circle.physicsBody!.affectedByGravity = false
        self.addChild(circle)
        
        // set the name in the circle
        
        
        
        // set the background color
//        self.backgroundColor = UIColor(
//            red:1,
//            green:0.98,
//            blue:1,
//            alpha:1)
//        let bgTexture = SKTexture(imageNamed: "images.jpeg")
//        for var i:CGFloat=0; i<3; i++ {
//            
//            bg = SKSpriteNode(texture: bgTexture)
//            bg.position = CGPoint(x: bgTexture.size().width/2 + bgTexture.size().width * i, y: CGRectGetMidY(self.frame))
//            bg.size.height = self.frame.height
//            
//            
//            self.addChild(bg)
//        }

        self.addChild(base)
        base.position = CGPointMake(500,200)
        base.zPosition = 5
        self.addChild(ball)
        ball.position = base.position
        ball.zPosition = 6
        // set the scale of the joystick
        base.xScale = 0.3
        base.yScale = 0.3
        ball.xScale = 0.3
        ball.yScale = 0.3
        base.alpha = 0
        ball.alpha = 0
        
        // add camera
        
        
//        print(camer.position.x)
//        print(camer.position.y)
//        print(self.frame.width)
//        print(self.frame.height)
//        print(circlex.position.x)
//        print(circlex.position.y)
        self.addChild(camer)
        self.camera = camer
        self.camera?.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))
        self.camera?.xScale = 1
        self.camera?.yScale = 1
        
        
        // add grounds
        let ground1 = SKShapeNode(rectOfSize: CGSize(width: 1000, height: 1))
        ground1.position = CGPointMake(500, 0)
        ground1.alpha = 0;
        ground1.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(1000, 1))
        ground1.physicsBody!.dynamic = false
        ground1.physicsBody!.categoryBitMask = BodyType.levelGround.rawValue
        self.addChild(ground1)
        
        let ground2 = SKShapeNode(rectOfSize: CGSize(width: 1, height: 1000))
        ground2.position = CGPointMake(1000, 500)
        ground2.strokeColor = SKColor.orangeColor()
        ground2.fillColor = SKColor.orangeColor()
        ground2.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(1, 1000))
        ground2.physicsBody!.dynamic = false
        ground2.physicsBody!.categoryBitMask = BodyType.verticalGround.rawValue
        ground2.physicsBody!.contactTestBitMask = BodyType.circle.rawValue
        self.addChild(ground2)
        
        let ground3 = SKShapeNode(rectOfSize: CGSize(width: 1000, height: 1))
        ground3.position = CGPointMake(500, 1000)
        ground3.strokeColor = SKColor.orangeColor()
        ground3.fillColor = SKColor.orangeColor()
        ground3.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(1000, 1))
        ground3.physicsBody!.dynamic = false
        ground3.physicsBody!.categoryBitMask = BodyType.levelGround.rawValue
        self.addChild(ground3)
        
        let ground4 = SKShapeNode(rectOfSize: CGSize(width: 1, height: 1000))
        ground4.position = CGPointMake(0, 500)
        ground4.strokeColor = SKColor.orangeColor()
        ground4.fillColor = SKColor.orangeColor()
        ground4.physicsBody = SKPhysicsBody(rectangleOfSize: CGSizeMake(1, 1000))
        ground4.physicsBody!.dynamic = false
        ground4.physicsBody!.categoryBitMask = BodyType.verticalGround.rawValue
        ground4.physicsBody!.contactTestBitMask = BodyType.circle.rawValue
        self.addChild(ground4)
        
        motionManager.accelerometerUpdateInterval = 0.1
        motionManager.startAccelerometerUpdates()
        motionManager.startMagnetometerUpdates()
        motionManager.startDeviceMotionUpdates()
            }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        ball.alpha = 0.4
        base.alpha = 0.4
        
        for touch in (touches ) {
            let location = touch.locationInNode(self)
            if (CGRectContainsPoint(button1.frame, location)) {
                stickActive = false;
//                print("hihi");
                // split function
            }else {
                if(accelerometer != true) {
            stickActive = true
            base.position = location
            ball.position = location
            circleSpeedX = 0
            circleSpeedY = 0
            }
            }
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        for touch in (touches ) {
            if(accelerometer != true) {
            let location = touch.locationInNode(self)
            if (stickActive == true) {
                
                let v = CGVector(dx: location.x - base.position.x, dy:  location.y - base.position.y)
                let angle = atan2(v.dy, v.dx)
                
                let length:CGFloat = base.frame.size.height / 2
                
                let xDist:CGFloat = sin(angle - 1.57079633) * length
                let yDist:CGFloat = cos(angle - 1.57079633) * length
                
                
                if (CGRectContainsPoint(base.frame, location)) {
                    
                    ball.position = location
                    
                } else {
                    
                    ball.position = CGPointMake( base.position.x - xDist, base.position.y + yDist)
                    
                }
                
                //circle.zRotation = angle - 1.57079633
                
                // set up the speed
                
                // change the speed according to the radius
                let multiplier:CGFloat = 300
                
                
                circleSpeedX = sin(angle - 1.57079633) * multiplier * (1/circle.radius)
                circleSpeedY = cos(angle - 1.57079633) * multiplier * (1/circle.radius)
//                print(circleSpeedX)
//                print(circleSpeedY)
            }
            }
        }
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        
        let contactMask = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
//        print(contactMask)
//        print(BodyType.circle.rawValue | BodyType.verticalGround.rawValue)
//        print(BodyType.circle.rawValue | BodyType.levelGround.rawValue)
//        print("************")
        switch(contactMask) {
            
        case BodyType.circle.rawValue | BodyType.levelGround.rawValue:
            //either the contactMask was the bro type or the ground type
            circleSpeedY = -circleSpeedY
//            print(circleSpeedX)
//            print(circleSpeedY)
        case BodyType.circle.rawValue | BodyType.verticalGround.rawValue:
            //either the contactMask was the bro type or the ground type
            circleSpeedX = -circleSpeedX
//            print(circleSpeedX)
//            print(circleSpeedY)

        
        default:
            return
            
        }
    }


   
    override func update(currentTime: CFTimeInterval) {
        if(accelerometer) {
            if motionManager.accelerometerData != nil {
                moveCircleFromAcceleration()
            }
            
        }
        if motionManager.deviceMotion != nil{
            print(motionManager.deviceMotion!.attitude.roll)
            if motionManager.deviceMotion!.attitude.roll < 1.5 {
                self.backgroundColor = SKColor.whiteColor()
            } else {
                self.backgroundColor = SKColor.blackColor()
            }
        }


        circle.position = CGPointMake(circle.position.x - circleSpeedX, circle.position.y + circleSpeedY)
        self.camera!.position = circle.position
        button1.position = CGPointMake(circle.position.x + 266.5, circle.position.y - 87.5)
//        print(circle.position)
//        print(self.anchorPoint)
//        print(self.frame.width)
//        print(self.frame.height)
        


            }
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if(accelerometer != true){
        if (stickActive == true) {
            
            let move:SKAction = SKAction.moveTo(base.position, duration: 0.2)
            move.timingMode = .EaseOut
            
            ball.runAction(move)
            
            let fade:SKAction = SKAction.fadeAlphaTo(0, duration: 0.2)
            
            ball.runAction(fade)
            base.runAction(fade)
            
             //circleSpeedX = 0
             //circleSpeedY = 0
            }
        }
        
    }
    
    func moveCircleFromAcceleration() {
        var accel2D = CGPoint.zero
        
        if motionManager.accelerometerData == nil { print("no acceleration data yet"); return
        }
        var raw = Vector3(
            x: CGFloat(motionManager.accelerometerData!.acceleration.x), y: CGFloat(motionManager.accelerometerData!.acceleration.y), z: CGFloat(motionManager.accelerometerData!.acceleration.z))
       accel2D.x = Vector3.dotProduct(raw, right: az)
        accel2D.y = Vector3.dotProduct(raw, right: ax)
        accel2D.normalize()
        
        if abs(accel2D.x) < steerDeadZone { accel2D.x = 0
        }
        if abs(accel2D.y) < steerDeadZone { accel2D.y = 0
        }
        
        let multiplier:CGFloat = 300
        
        let angle = atan2(accel2D.y, accel2D.x)
        
        circleSpeedX = sin(angle - 1.57079633) * multiplier * (1/circle.radius)
        circleSpeedY = cos(angle - 1.57079633) * multiplier * (1/circle.radius)
        
    }

    

}
