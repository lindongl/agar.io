//
//  food.swift
//  agar.io
//
//  Created by lld on 15/10/8.
//  Copyright © 2015年 Big Nerd Ranch. All rights reserved.
//

import Foundation
import SpriteKit
class food: SKShapeNode {
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init (position: CGPoint) {
        let length:CGFloat = cos(CGFloat(Float(M_PI) / 6)) * 10;
        var hexagon = CGPathCreateMutable();
        CGPathMoveToPoint(hexagon,nil , 0, 0)
        CGPathAddLineToPoint(hexagon,nil,length, 5)
        CGPathAddLineToPoint(hexagon,nil, length, 15)
        CGPathAddLineToPoint(hexagon,nil, 0, 20)
        CGPathAddLineToPoint(hexagon,nil, -length, 15)
        CGPathAddLineToPoint(hexagon,nil, -length, 5)
        super.init()
        self.path = hexagon
        self.position = position
    }

}