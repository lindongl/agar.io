//
//  GameViewController.swift
//  agar.io
//
//  Created by lld on 15/9/20.
//  Copyright (c) 2015年 Big Nerd Ranch. All rights reserved.
//

import UIKit
import SpriteKit
import CoreMotion

class GameViewController: UIViewController {
    

    override func viewDidLoad() {
        super.viewDidLoad()

        let scene =
        MainMenuScene(size:CGSize(width: 2048, height: 1536))            // Configure the view.
            let skView = self.view as! SKView
            skView.showsFPS = true
            skView.showsNodeCount = true
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            //scene.size = skView.bounds.size
            
            skView.presentScene(scene)
//        motionManager.accelerometerUpdateInterval = 0.05
//        motionManager.startAccelerometerUpdates()
    }

    override func shouldAutorotate() -> Bool {
        return true
    }

    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return .AllButUpsideDown
        } else {
            return .All
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
           
}
