//
//  SizeableCircle.swift
//  agar.io
//
//  Created by lld on 15/10/5.
//  Copyright © 2015年 Big Nerd Ranch. All rights reserved.
//
import SpriteKit
import Foundation
class SizeableCircle: SKShapeNode {
    
    var radius: CGFloat {
        didSet {
            self.path = SizeableCircle.path(self.radius)
        }
    }
    
    init(radius: CGFloat, position: CGPoint) {
        self.radius = radius
        
        super.init()
        
        self.path = SizeableCircle.path(self.radius)
        self.position = position
    }
    
    override init() {
        self.radius = 0;
        super.init()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    class func path(radius: CGFloat) -> CGMutablePathRef {
        var path: CGMutablePathRef = CGPathCreateMutable()
        CGPathAddArc(path, nil, 0.0, 0.0, radius, 0.0, CGFloat(Float(M_PI) * 2.0), true)
        return path
    }
    
}